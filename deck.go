package main

import (
	"io/ioutil"
	"strings" //  package used in convertn=ing string to byte
)

//Create a new type of deck
// which is a Slice of strings

//type deck []string

//Function to creat a new deck of cards
// New deck of cards will b of the data type deck (created above)
// Deck of card combo is created out of a combination of shapes/ color and numbers
// Order of type conversion: deck--> [] string (which is a slice) --> string --> []byte (byte slice)

func newDeck() deck {
	cards := deck{}                                                //declare  new variable cards and assign it to type deck; all new cards will bear the data type deck
	cardSuits := []string{"spades", "diamonds", "clubs", "hearts"} //declare cardSuits as string and create a slice with 4 card types
	cardValues := []string{"Ace", "two", "three", "four"}          // delcare cardValues as string and create 4 card numbers

	// create a deck of cards of ALL combinations created from cardSuits and cardValues
	// create a for loop until the function spits out all possible combinations of cards
	for _, suit := range cardSuits {
		for _, value := range cardValues {
			cards = append(cards, value+" of "+suit) //append to a new card name by using string concatenation
			//fmt.Println(cards)                       // print all cards on a single line
		}

	}
	return cards //calling a newDeck func will return all card combos
}

// Create a receiver function (d deck) which will return a card of type deck (strings)
// prints the value/ shape of each card
func (d deck) print() {
	for i, card := range d {
		println(i, card)
	}
}

// Function to create a deal
// deal function will have deck data type and int (handsize)

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:] //return func to declare the byte size; starts from index 0 (undeclared) and ends at handSize to the last card
}

// Function to create and onvert the return value if cards from func deck (type deck) into string
// the return value of this func will be called on the main.go file
func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

// Function to save the cards data on to a file
//
func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}
